package memo.shared.model

import java.util.*

/**Модель заметки*/
class NoteModel(
	var remoteId: Long,
	var localId: String = UUID.randomUUID().toString(),
	/**
	 * Заголовок заметки. Лимит в 100 символов.
	 * */
	var title: String?,
	/**
	 * Текст заметки. Лимит в 5000 символов.
	 * */
	var text: String?,
	/**
	 * Ссылка на картинку
	 * */
	var imageUrl: String?,
	/**
	 * Картинка, представленная как массив байтов
	 * */
	var image: ByteArray?,
	/**
	 * Дата создания заметки. Количество секунд с 1 января 1970 года.
	 * */
	var date: Long,
	/**
	 * Настроение пользователя. Число от 0 до 4.
	 * */
	var mood: Byte,
	/**
	 * Стоит ли заметка на удаление.
	 * */
	var toDelete: Boolean
)