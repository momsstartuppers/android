package network

import memo.diary.network_api.NetworkApi
import network.mapper.ModelConverterImpl

/**Класс, предоставляющий объекты для работы с сетью*/
class NetworkProvider {
	companion object {
		/**
		 * Создать объект [NetworkApi] для работы с сетью
		 *
		 * @param token - токен пользователя для подтверждения операций на сервере
		 * @return объект [NetworkApi]
		 * */
		fun getNetworkApi(token: String): NetworkApi {
			val api = RetrofitProvider.getClient(token).create(RetrofitApi::class.java)
			val converter = ModelConverterImpl()
			return NetworkApiImpl(api, converter)
		}
	}
}