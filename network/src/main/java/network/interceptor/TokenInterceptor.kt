package network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

internal class TokenInterceptor(private val token: String) : Interceptor {
	override fun intercept(chain: Interceptor.Chain): Response {
		val headerWithToken = chain
			.request()
			.newBuilder()
			.header("Authorization", token)
			.build()
		return chain.proceed(headerWithToken)
	}
}