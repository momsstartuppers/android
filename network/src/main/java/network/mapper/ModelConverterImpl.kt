package network.mapper

import memo.diary.network_api.NetworkApiConverterException
import memo.diary.network_api.NetworkResponse
import memo.diary.network_api.model.request.Note
import memo.diary.network_api.model.response.*
import memo.shared.model.NoteModel
import network.model.request.RetrofitNote
import network.model.request.RetrofitNotesToDelete
import network.model.response.*
import retrofit2.Response

private const val CONVERTER_EXCEPTION = "Не удалось конвертировать модель"

internal class ModelConverterImpl : ModelConverter {
	private val invokeConverterError = { throw NetworkApiConverterException(CONVERTER_EXCEPTION) }

	override fun convertNote(model: Note): RetrofitNote =
		RetrofitNote(
			date = model.date,
			mood = model.mood,
			localId = model.localId,
			remoteId = model.remoteId,
			text = model.text,
			title = model.title
		)

	override fun convertNotesToDelete(model: List<Long>): RetrofitNotesToDelete = RetrofitNotesToDelete(model)

	override fun convertImageUploadResponse(model: Response<RetrofitImageUploadResponse>): NetworkResponse<ImageUploadResponse> =
		NetworkResponse(
			responseCode = model.code(),
			model = ImageUploadResponse(model.body()?.image ?: invokeConverterError())
		)

	override fun convertListOfNotesResponse(model: Response<RetrofitListOfNotesResponse>): NetworkResponse<ListOfNotesResponse> {
		if (model.body() == null || model.body()!!.notesId == null || model.body()!!.updated == null) invokeConverterError()

		val updatedList = mutableListOf<NoteModel>()
		for (note in model.body()!!.updated!!) {
			if (note != null) {
				updatedList.add(
					NoteModel(

						date = note.date ?: invokeConverterError(),
						image = null,
						imageUrl = note.image ?: invokeConverterError(),
						localId = note.localId ?: invokeConverterError(),
						mood = note.mood ?: invokeConverterError(),
						remoteId = note.remoteId ?: invokeConverterError(),
						text = note.text ?: invokeConverterError(),
						title = note.title ?: invokeConverterError(),
						toDelete = false
					)
				)
			} else {
				invokeConverterError()
			}
		}

		return NetworkResponse(
			responseCode = model.code(),
			model = ListOfNotesResponse(
				notesId = model.body()!!.notesId!!,
				updated = updatedList
			)
		)

	}

	override fun convertNoteResponse(model: Response<RetrofitNoteResponse>): NetworkResponse<NoteModel> {
		if (model.body() == null) invokeConverterError()
		return NetworkResponse(
			responseCode = model.code(),
			model = NoteModel(
				date = model.body()!!.date ?: invokeConverterError(),
				image = null,
				imageUrl = model.body()!!.image ?: invokeConverterError(),
				localId = model.body()!!.localId ?: invokeConverterError(),
				mood = model.body()!!.mood ?: invokeConverterError(),
				remoteId = model.body()!!.remoteId ?: invokeConverterError(),
				text = model.body()!!.text ?: invokeConverterError(),
				title = model.body()!!.title ?: invokeConverterError(),
				toDelete = false
			)
		)
	}

	override fun convertNoteUpdateResponse(model: Response<RetrofitNoteUpdateResponse>): NetworkResponse<NoteUpdateResponse> =
		NetworkResponse(
			responseCode = model.code(),
			model = NoteUpdateResponse(
				localId = model.body()!!.localId ?: invokeConverterError(),
				remoteId = model.body()!!.remoteId ?: invokeConverterError()
			)
		)

	override fun convertServerTimeResponse(model: Response<RetrofitServerTimeResponse>): NetworkResponse<ServerTimeResponse> =
		NetworkResponse(
			responseCode = model.code(),
			model = ServerTimeResponse(model.body()!!.serverTime ?: invokeConverterError())
		)

	override fun convertVoid(model: Response<Void>): NetworkResponse<EmptyResponse> =
		NetworkResponse(
			responseCode = model.code(),
			model = EmptyResponse()
		)
}

