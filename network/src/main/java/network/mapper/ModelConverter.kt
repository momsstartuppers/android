package network.mapper

import memo.diary.network_api.NetworkApiConverterException
import memo.diary.network_api.NetworkResponse
import memo.diary.network_api.model.request.Note
import memo.diary.network_api.model.response.*
import memo.shared.model.NoteModel
import network.model.request.RetrofitNote
import network.model.request.RetrofitNotesToDelete
import network.model.response.*
import retrofit2.Response

internal interface ModelConverter {
	//region request
	fun convertNote(model: Note): RetrofitNote

	fun convertNotesToDelete(model: List<Long>): RetrofitNotesToDelete
	//endregion

	//region response
	@Throws(NetworkApiConverterException::class)
	fun convertImageUploadResponse(model: Response<RetrofitImageUploadResponse>): NetworkResponse<ImageUploadResponse>

	@Throws(NetworkApiConverterException::class)
	fun convertListOfNotesResponse(model: Response<RetrofitListOfNotesResponse>): NetworkResponse<ListOfNotesResponse>

	@Throws(NetworkApiConverterException::class)
	fun convertNoteResponse(model: Response<RetrofitNoteResponse>): NetworkResponse<NoteModel>

	@Throws(NetworkApiConverterException::class)
	fun convertNoteUpdateResponse(model: Response<RetrofitNoteUpdateResponse>): NetworkResponse<NoteUpdateResponse>

	@Throws(NetworkApiConverterException::class)
	fun convertServerTimeResponse(model: Response<RetrofitServerTimeResponse>): NetworkResponse<ServerTimeResponse>

	fun convertVoid(model: Response<Void>): NetworkResponse<EmptyResponse>
	//endregion
}