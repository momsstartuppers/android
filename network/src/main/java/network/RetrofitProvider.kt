package network

import io.reactivex.schedulers.Schedulers
import network.interceptor.TokenInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

internal class RetrofitProvider {

	companion object {
		fun getClient(token: String): Retrofit {
			val retrofit: Retrofit
			val builder = OkHttpClient.Builder()
			builder.addNetworkInterceptor(TokenInterceptor(token))
			val client = builder.build()

			retrofit = Retrofit.Builder()
				.baseUrl("https://www.bestdiary.ru/")
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
				.client(client)
				.build()

			return retrofit
		}
	}
}
