package network

import io.reactivex.Single
import memo.diary.network_api.NetworkApi
import memo.diary.network_api.NetworkResponse
import memo.diary.network_api.model.request.Note
import memo.diary.network_api.model.response.*
import memo.shared.model.NoteModel
import network.mapper.ModelConverter
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

internal class NetworkApiImpl(private val api: RetrofitApi, private val converter: ModelConverter) : NetworkApi {
	override fun getServerTime(): Single<NetworkResponse<ServerTimeResponse>> =
		api.getServerTime()
			.map { converter.convertServerTimeResponse(it) }

	override fun getNote(remoteId: Long): Single<NetworkResponse<NoteModel>> =
		api.getNote(remoteId)
			.map { converter.convertNoteResponse(it) }

	override fun postNote(note: Note): Single<NetworkResponse<NoteUpdateResponse>> =
		api.postNote(converter.convertNote(note))
			.map { converter.convertNoteUpdateResponse(it) }

	override fun updateNote(note: Note): Single<NetworkResponse<NoteUpdateResponse>> =
		api.updateNote(converter.convertNote(note))
			.map { converter.convertNoteUpdateResponse(it) }

	override fun deleteNote(remoteId: Long): Single<NetworkResponse<EmptyResponse>> =
		api.deleteNote(remoteId)
			.map { converter.convertVoid(it) }

	override fun deleteListOfNotes(notesToDelete: List<Long>): Single<NetworkResponse<EmptyResponse>> =
		api.deleteListOfNotes(converter.convertNotesToDelete(notesToDelete))
			.map { converter.convertVoid(it) }

	override fun getListOfNotes(beginDate: Long, endDate: Long): Single<NetworkResponse<ListOfNotesResponse>> =
		api.getListOfNotes(beginDate, endDate)
			.map { converter.convertListOfNotesResponse(it) }

	override fun uploadImage(remoteId: Long, image: File): Single<NetworkResponse<ImageUploadResponse>> {
		val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image)
		val bodyWithImage = MultipartBody.Part.createFormData("image", image.name, requestFile)
		return api.uploadImage(remoteId, bodyWithImage)
			.map { converter.convertImageUploadResponse(it) }
	}

	override fun getImage(remoteId: Long): Single<NetworkResponse<ImageUploadResponse>> =
		api.getImage(remoteId)
			.map { converter.convertImageUploadResponse(it) }

	override fun deleteImage(remoteId: Long): Single<NetworkResponse<EmptyResponse>> =
		api.deleteImage(remoteId)
			.map { converter.convertVoid(it) }
}