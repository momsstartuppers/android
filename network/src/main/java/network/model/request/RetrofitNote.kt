package network.model.request

import com.google.gson.annotations.SerializedName

internal data class RetrofitNote(
	@field:SerializedName("date")
	val date: Long,

	@field:SerializedName("mood")
	val mood: Int,

	@field:SerializedName("local_id")
	val localId: String? = null,

	@field:SerializedName("remote_id")
	val remoteId: Long? = null,

	@field:SerializedName("text")
	val text: String,

	@field:SerializedName("title")
	val title: String
)