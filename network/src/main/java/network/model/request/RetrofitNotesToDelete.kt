package network.model.request

import com.google.gson.annotations.SerializedName

internal data class RetrofitNotesToDelete(

    @field:SerializedName("notes_to_delete")
    val notesToDelete: List<Long>
)