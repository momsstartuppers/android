package network.model.response

import com.google.gson.annotations.SerializedName

internal data class RetrofitImageUploadResponse(

	@field:SerializedName("image")
	val image: String? = null
)