package network.model.response

import com.google.gson.annotations.SerializedName

internal data class RetrofitListOfNotesResponse(
    @field:SerializedName("notes_id")
    val notesId: List<Long?>? = null,

    @field:SerializedName("updated")
    val updated: List<RetrofitNoteResponse?>? = null
)