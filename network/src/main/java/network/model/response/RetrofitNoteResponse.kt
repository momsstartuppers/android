package network.model.response

import com.google.gson.annotations.SerializedName

internal data class RetrofitNoteResponse(
	@field:SerializedName("date")
    val date: Long? = null,

	@field:SerializedName("image")
    val image: String? = null,

	@field:SerializedName("local_id")
    val localId: String? = null,

	@field:SerializedName("mood")
	val mood: Byte? = null,

	@field:SerializedName("remote_id")
    val remoteId: Long? = null,

	@field:SerializedName("text")
    val text: String? = null,

	@field:SerializedName("title")
    val title: String? = null
)