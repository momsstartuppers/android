package network.model.response


import com.google.gson.annotations.SerializedName

internal data class RetrofitNoteUpdateResponse(

	@field:SerializedName("local_id")
	val localId: String? = null,

	@field:SerializedName("remote_id")
	val remoteId: Long? = null
)