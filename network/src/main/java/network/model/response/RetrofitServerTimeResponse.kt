package network.model.response

import com.google.gson.annotations.SerializedName

internal data class RetrofitServerTimeResponse(

	@field:SerializedName("server_time")
	val serverTime: Long? = null
)