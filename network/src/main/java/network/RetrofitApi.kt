package network

import io.reactivex.Single
import network.model.request.RetrofitNote
import network.model.request.RetrofitNotesToDelete
import network.model.response.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

const val NOTE_URL = "api/note"
const val NOTES_URL = "api/notes"
const val IMAGE_URL = "api/note/image"

internal interface RetrofitApi {
	/**
	 * Возвращает серверное время
	 * */
	@GET("api/get_server_time/")
	fun getServerTime(): Single<Response<RetrofitServerTimeResponse>>

	/**
	 * Получить заметку с сервера
	 * @param remoteId - id заметки на сервере
	 * */
	@GET(NOTE_URL)
	fun getNote(@Query("remote_id") remoteId: Long): Single<Response<RetrofitNoteResponse>>

	/**
	 * Отправить заметку на сервер
	 * */
	@POST(NOTE_URL)
	fun postNote(@Body note: RetrofitNote): Single<Response<RetrofitNoteUpdateResponse>>

	/**
	 * Обновить заметку
	 * @param note - заметка, которую следует обновить
	 * */
	@HTTP(method = "PATCH", path = NOTE_URL, hasBody = true)
	fun updateNote(@Body note: RetrofitNote): Single<Response<RetrofitNoteUpdateResponse>>

	/**
	 * Удалить заметку с выбранным id
	 * @param remoteId - id заметки на сервере
	 * */
	@HTTP(method = "DELETE", path = NOTE_URL, hasBody = false)
	fun deleteNote(@Query("remote_id") remoteId: Long): Single<Response<Void>>

	/**
	 * Удалить список заметок
	 * @param notesToDelete - список заметок на удаление
	 * */
	@HTTP(method = "DELETE", path = NOTES_URL, hasBody = true)
	fun deleteListOfNotes(@Body notesToDelete: RetrofitNotesToDelete): Single<Response<Void>>

	/**
	 * Получить список заметок
	 * @param beginDate - ???
	 * @param endDate - ???
	 */
	@GET(NOTES_URL)
	fun getListOfNotes(@Query("begin_date") beginDate: Long, @Query("end_date") endDate: Long): Single<Response<RetrofitListOfNotesResponse>>

	/**
	 * Загрузить картинку на сервер
	 * @param remoteId - id заметки, хранящейся на сервере
	 * @param image - картинка, загружаемая на сервер
	 * */
	@Multipart
	@PUT(IMAGE_URL)
	fun uploadImage(@Query("remote_id") remoteId: Long, @Part image: MultipartBody.Part): Single<Response<RetrofitImageUploadResponse>>

	/**
	 * Получить картинку с сервера
	 * @param remoteId - id заметки, хранящейся на сервере
	 * */
	@GET(IMAGE_URL)
	fun getImage(@Query("remote_id") remoteId: Long): Single<Response<RetrofitImageUploadResponse>>

	/**
	 * Удалить картинку с сервера
	 * @param remoteId - id картинки на сервере
	 * */
	@DELETE(IMAGE_URL)
	fun deleteImage(@Query("remote_id") remoteId: Long): Single<Response<Void>>
}