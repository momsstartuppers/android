package memo.diary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import memo.diary.screen.notes.NotesScreen

class SingleActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_single)
		setupFragment()
	}

	fun setupFragment() {
		if (supportFragmentManager.findFragmentById(R.id.rootContainer) == null) {
			supportFragmentManager.beginTransaction()
				.add(R.id.rootContainer, NotesScreen())
				.commit()
		}
	}
}
