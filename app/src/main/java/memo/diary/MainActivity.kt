package memo.diary

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.startActivityForResult
import androidx.core.content.ContextCompat
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import memo.diary.network_api.model.request.Note
import storage.Storage
import network.NetworkProvider
import java.io.File
import java.util.*

const val RC_SIGN_IN = 100
const val RC_READ_PERM = 101
const val RC_IMAGE = 102
const val DEBUG_KEY = "myDebugKey"
const val TEST_TOKEN = "mitrofanov_test_user"

class MainActivity : AppCompatActivity() {

	private lateinit var appStorage: Storage
	private val networkApi = NetworkProvider.getNetworkApi(TEST_TOKEN)
	private var imageURI: Uri? = null
	private val debug = { it: String -> Log.d(this.localClassName, it) }

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		login.setOnClickListener { firebaseAuth() }
		logout.setOnClickListener { firebaseLogout() }
		permission.setOnClickListener { checkPermisssions() }
		serverTime.setOnClickListener { getServerTime() }
		sendNote.setOnClickListener { sendNote() }
		updateNote.setOnClickListener { updateNote() }
		deleteNote.setOnClickListener { deleteNote() }
		deleteNotes.setOnClickListener { deleteNotes() }
		getListOfNotes.setOnClickListener { getListOfNotes() }
		sendImage.setOnClickListener { sendImage() }
		pickImage.setOnClickListener { pickImage() }
		getImage.setOnClickListener { getImage() }
		deleteImage.setOnClickListener { deleteImage() }

		initStorage()
	}

	fun firebaseAuth() {
		val providers = arrayListOf(
			AuthUI.IdpConfig.EmailBuilder().build(),
			AuthUI.IdpConfig.GoogleBuilder().build()
		)

		startActivityForResult(
			this,
			AuthUI.getInstance()
				.createSignInIntentBuilder()
				.setAvailableProviders(providers)
				.build(),
			RC_SIGN_IN, null
		)
	}

	fun firebaseLogout() {
		AuthUI.getInstance()
			.signOut(this)
			.addOnCompleteListener {
				if (it.isSuccessful) Log.d(DEBUG_KEY, "logout success")
				else Log.d(DEBUG_KEY, "logout failed")
			}
	}

	fun initStorage() {
		val tokenStorage = storage.TokenStorage(applicationContext)
		val databaseStorage = storage.Database()
		appStorage = Storage(tokenStorage, databaseStorage)
	}

	fun checkPermisssions() {
		if (ContextCompat.checkSelfPermission(
				this,
				Manifest.permission.READ_EXTERNAL_STORAGE
			) == PackageManager.PERMISSION_GRANTED
		) {
			Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show()
		} else {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
				//объяснения зачем нужны разрешения
				Toast.makeText(this, "need read permissions", Toast.LENGTH_SHORT).show()
				ActivityCompat.requestPermissions(
					this,
					arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
					RC_READ_PERM
				)
				Log.d(DEBUG_KEY, "need permission explanation")
			} else {
				//просим разрешения
				ActivityCompat.requestPermissions(
					this,
					arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
					RC_READ_PERM
				)
				Log.d(DEBUG_KEY, "get permissions")
			}
		}
	}

	@SuppressLint("CheckResult")
	fun getServerTime() {
		networkApi.getServerTime().subscribe(
			{
				debug("getServerTime success")
				debug("getServerTime: ${it.model.serverTime}")
			},
			{
				debug("getServerTime error")
				it.printStackTrace()
			}
		)
	}

	@SuppressLint("CheckResult")
	fun sendNote() {
		val note = Note(
			date = Calendar.getInstance().timeInMillis / 1000,
			mood = 0,
			localId = "someLocalId",
			text = "Томат - не человек",
			title = "Покажи сосочек - подарю цветочек",
			remoteId = null
		)

		networkApi.postNote(note)
			.subscribe(
				{
					debug("sendNote success")
					debug("sendNote response code: ${it.responseCode}")
				},
				{
					debug("sendNote error")
					it.printStackTrace()
				})
	}

	@SuppressLint("CheckResult")
	fun updateNote() {
		val note = Note(
			date = Calendar.getInstance().timeInMillis / 1000,
			mood = 3,
			localId = "asdasd",
			remoteId = 270,
			text = "Томат - человек",
			title = "Лёгкая эротика"
		)

		networkApi.updateNote(note).subscribe(
			{
				debug("updateNote success")
				debug("updateNote response code: ${it.responseCode}")
			},
			{
				debug("updateNote error")
				it.printStackTrace()
			}
		)
	}

	@SuppressLint("CheckResult")
	fun deleteNote() {
		networkApi.deleteNote(270).subscribe(
			{
				debug("deleteNote success")
				debug("deleteNote response code: ${it.responseCode}")
			},
			{
				debug("deleteNote error")
				it.printStackTrace()
			}
		)
	}

	@SuppressLint("CheckResult")
	fun deleteNotes() {
		networkApi.deleteListOfNotes(listOf(273, 274))
			.subscribe(
				{
					debug("deleteNotes success")
					debug("deleteNotes response code: ${it.responseCode}")
				},
				{
					debug("deleteNotes error")
					it.printStackTrace()
				}
			)
	}

	fun getAbsoluteFilePath(uri: Uri): String {
		var cursor: Cursor? = null
		try {
			val proj = arrayOf(MediaStore.Images.Media.DATA)
			cursor = contentResolver.query(uri, proj, null, null, null)
			val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
			cursor.moveToFirst()
			return cursor.getString(columnIndex)
		} finally {
			cursor?.close()
		}
	}

	@SuppressLint("CheckResult")
	fun getListOfNotes() {
		networkApi.getListOfNotes(0, Calendar.getInstance().timeInMillis / 1000)
			.subscribe(
				{
					debug("getListOfNotes: success")
					debug("getListOfNotes: response code: ${it.responseCode}")
				},
				{
					debug("getListOfNotes error")
					it.printStackTrace()
				}
			)
	}

	fun sendImage() {
		imageURI?.let {
			val filePath = getAbsoluteFilePath(it)
			val file = File(filePath)

			networkApi.uploadImage(remoteId = 280, image = file).subscribe(
				{ networkResponse ->
					debug("sendImage success")
					debug("sendImage response code: ${networkResponse.responseCode}")
				},
				{
					debug("sendImage error")
				}
			)
		} ?: debug("sendImage: imageUri is null")
	}

	fun pickImage() {
		val getIntent = Intent(Intent.ACTION_GET_CONTENT)
		getIntent.type = "image/*"

		val pickIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
		pickIntent.type = "image/*"

		val chooserIntent = Intent.createChooser(getIntent, "Select Image")
		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

		startActivityForResult(chooserIntent, RC_IMAGE)
	}

	@SuppressLint("CheckResult")
	fun getImage() {
		networkApi.getImage(280).subscribe(
			{
				debug("getImage success")
				debug("getImage response code: ${it.responseCode}")
			},
			{
				debug("getImage error")
				it.printStackTrace()
			}
		)
	}

	@SuppressLint("CheckResult")
	fun deleteImage() {
		networkApi.deleteImage(280)
			.subscribe(
				{
					debug("deleteImage success")
					debug("deleteImage response code: ${it.responseCode}")
				},
				{
					debug("deleteImage error")
					it.printStackTrace()
				}
			)
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		when (requestCode) {
			RC_SIGN_IN -> {
				val response = IdpResponse.fromResultIntent(data)

				if (resultCode == Activity.RESULT_OK) {
					// Successfully signed in
					val user = FirebaseAuth.getInstance().currentUser
					Log.d(DEBUG_KEY, "auth success")

				} else {
					// Sign in failed. If response is null the user canceled the
					// sign-in flow using the back button. Otherwise check
					// response.getError().getErrorCode() and handle the error.
					// ...
					Log.d(DEBUG_KEY, "auth failed")
				}
			}

			RC_IMAGE -> {
				if (resultCode == Activity.RESULT_OK) {
					Log.d(DEBUG_KEY, "image pick success")
					imageURI = data?.data
					imageView.setImageURI(imageURI)
				} else {
					Log.d(DEBUG_KEY, "image pick failure")
				}
			}
		}
	}

	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		when (requestCode) {
			RC_READ_PERM -> {
				if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Toast.makeText(this, "permission granted", Toast.LENGTH_SHORT).show()
					Log.d(DEBUG_KEY, "permission granted")
				} else {
					Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show()
					Log.d(DEBUG_KEY, "permission denied")
				}
			}
		}
	}
}




