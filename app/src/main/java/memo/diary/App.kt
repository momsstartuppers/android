package memo.diary

import android.app.Application
import com.google.firebase.analytics.FirebaseAnalytics
import io.realm.Realm

class App : Application() {
	override fun onCreate() {
		super.onCreate()
		Realm.init(this)
		FirebaseAnalytics.getInstance(this)
	}
}