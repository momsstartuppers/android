package memo.diary.screen.notes

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_notes_screen.*
import memo.diary.R
import network.NetworkProvider
import storage.Database
import storage.adapter.NotesAdapter
import java.util.*

class NotesScreen : Fragment() {
	private val database = Database()
	private var calendarView: BottomSheetDialog? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		initCalendarView()
	}

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_notes_screen, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		bottomBar.apply {
			inflateMenu(R.menu.notes_screen_bottom_menu)
			fab.setOnClickListener {
				Log.d("fuck", "fab clicked")
				downloadItems()
			}

			setOnMenuItemClickListener {
				when (it.itemId) {
					R.id.calendar -> {
						calendarView?.show()
						true
					}
					R.id.search -> true
					else -> false
				}
			}

			setNavigationOnClickListener {
				clearData()
			}
		}
	}

	override fun onStart() {
		database.open()
		initRV()
		super.onStart()
	}

	override fun onStop() {
		database.close()
		super.onStop()
	}

	//инициализация RecyclerView
	private fun initRV() {
		if (list.adapter == null) {
			list.apply {
				setHasFixedSize(true)
				layoutManager = LinearLayoutManager(this@NotesScreen.context)
				adapter = NotesAdapter(database.getOrderedRealmCollection())
			}
		}
	}

	//инициализация CalendarView
	private fun initCalendarView() {
		if (calendarView == null) {
			context?.run {
				calendarView = BottomSheetDialog(this).apply {
					setContentView(R.layout.calendar)
				}
			}
		}
	}

	@SuppressLint("CheckResult")
	private fun downloadItems() {
		val api = NetworkProvider.getNetworkApi("mitrofanov_test_user")
		api.getListOfNotes(0, Calendar.getInstance().timeInMillis / 1000)
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(
				{
					Log.d("fuck", it.model.updated.toString())
					database.saveNotes(it.model.updated)
				},
				{
					Toast.makeText(context, "fucked up", Toast.LENGTH_SHORT).show()
				}
			)
	}

	private fun clearData() {
		database.realm.executeTransactionAsync {
			it.deleteAll()
		}
	}
}
