package api

/**Хранилище токена*/
interface TokenStorage {
    /**Сохранить токен*/
    fun saveToken(token: String)

    /**Получить токен из хранилища*/
    fun getToken(): String
}