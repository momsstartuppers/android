package api

import memo.shared.model.NoteModel

/**Хранилище данных*/
interface DatabaseStorage {
	/**
	 * Сохранить заметку в хранилище
	 * @param note - заметка на сохранение
	 * */
	fun saveNote(note: NoteModel)

	/**
	 * Сохранить список заметок в хранилище
	 * @param notes - список заметок на сохранение
	 * */
	fun saveNotes(notes: List<NoteModel>)

	/**
	 * Удалить заметку из хранилища
	 * @param id - идентификатор заметки на удаление
	 * */
	fun deleteNote(id: String)

	/**
	 * Удалить несколько заметок из хранилища
	 * @param ids - список из идентификаторов заметок на удаление
	 * */
	fun deleteNotes(ids: Array<String>)

	/**
	 * Пометить заметку для удаления в хранилище
	 * @param id - идентификатор заметки
	 * */
	fun markNoteOnDelete(id: String)

	/**
	 * Пометить заметки для удаления в хранилище
	 * @param ids - список идентификаторов заметок
	 * */
	fun markNotesOnDelete(ids: Array<String>)

	/**
	 * Получить список remoteId заметок на удаление
	 * */
	fun getNotesIdToDelete(): List<Long>
}