package storage

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**Схема базы данных*/
open class DatabaseModel(
	/**id заметки на сервере*/
	var remoteId: Long? = null,
	/**Первичный ключ*/
	@PrimaryKey var localId: String = "",
	/**Заголовок заметки. Лимит в 100 символов.*/
	var title: String? = null,
	/**Текст заметки. Лимит в 5000 символов.*/
	var text: String? = null,
	/**Ссылка на картинку*/
	var imageUrl: String? = null,
	/**Картинка, представленная как массив байтов*/
	var image: ByteArray? = null,
	/**Дата создания заметки. Количество секунд с 1 января 1970 года.*/
	var date: Long = 0,
	/**Настроение пользователя. Число от 0 до 4.*/
	var mood: Byte = 0,
	/**Должна ли заметка быть удалена с сервера*/
	var toDelete: Boolean = false
) : RealmObject()