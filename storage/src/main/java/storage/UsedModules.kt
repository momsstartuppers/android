package storage

import io.realm.annotations.RealmModule

@RealmModule(library = true, classes = [DatabaseModel::class])
open class UsedModules