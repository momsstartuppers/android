package storage

import api.DatabaseStorage

import io.realm.Case
import io.realm.OrderedRealmCollection
import io.realm.Realm
import io.realm.RealmConfiguration
import memo.shared.model.NoteModel

class Database : DatabaseStorage {
	lateinit var realm: Realm

	val realmDefaultConfig = RealmConfiguration.Builder()
		.name("NotesDatabase.realm")
		.deleteRealmIfMigrationNeeded()
		.modules(UsedModules()) //TODO разрешить проблему с использованием модулей в Realm
		.build()!!


	/**Открыть доступ к БД*/
	fun open() {
		realm = Realm.getInstance(realmDefaultConfig)
	}

	/**Закрыть базу данных*/
	fun close() {
		realm.close()
	}

	//region Методы для работы с БД
	/**
	 * Сохранить заметку в хранилище
	 * @param note - заметка на сохранение
	 * */
	override fun saveNote(note: NoteModel) {
		realm.executeTransactionAsync {
			it.insertOrUpdate(
				DatabaseModel(
					remoteId = note.remoteId,
					localId = note.localId,
					title = note.title,
					text = note.text,
					imageUrl = note.imageUrl,
					image = note.image,
					date = note.date,
					mood = note.mood,
					toDelete = note.toDelete
				)
			)
		}
	}

	/**
	 * Сохранить список заметок в хранилище
	 * @param notes - список заметок на сохранение
	 * */
	override fun saveNotes(notes: List<NoteModel>) {
		notes.map {
			DatabaseModel(
				remoteId = it.remoteId,
				localId = it.localId,
				title = it.title,
				text = it.text,
				imageUrl = it.imageUrl,
				image = it.image,
				date = it.date,
				mood = it.mood,
				toDelete = it.toDelete
			)
		}.run {
			realm.executeTransactionAsync {
				it.insertOrUpdate(this)
			}
		}
	}

	/**
	 * Удалить заметку из хранилища
	 * @param id - идентификатор заметки на удаление
	 * */
	override fun deleteNote(id: String) {
		realm.executeTransactionAsync {
			it.where(DatabaseModel::class.java)
				.equalTo("localId", id, Case.INSENSITIVE)
				.findFirst()
				?.deleteFromRealm()
		}
	}

	/**
	 * Удалить несколько заметок из хранилища
	 * @param ids - список из идентификаторов заметок на удаление
	 * */
	override fun deleteNotes(ids: Array<String>) {
		realm.executeTransactionAsync {
			it.where(DatabaseModel::class.java)
				.`in`("localId", ids, Case.INSENSITIVE)
				.findAll()
				?.deleteAllFromRealm()
		}
	}

	/**
	 * Пометить заметку для удаления в хранилище
	 * @param id - идентификатор заметки
	 * */
	override fun markNoteOnDelete(id: String) {
		realm.executeTransactionAsync {
			it.where(DatabaseModel::class.java)
				.equalTo("localId", id, Case.INSENSITIVE)
				.findFirst()
				?.run {
					toDelete = true
				}
		}
	}

	/**
	 * Пометить заметки для удаления в хранилище
	 * @param ids - список идентификаторов заметок
	 * */
	override fun markNotesOnDelete(ids: Array<String>) {
		realm.executeTransactionAsync {
			it.where(DatabaseModel::class.java)
				.`in`("localId", ids, Case.INSENSITIVE)
				.findAll()
				?.forEach { databaseModel ->
					databaseModel.toDelete = true
				}
		}
	}

	/**
	 * Получить список remoteId заметок на удаление
	 * */
	override fun getNotesIdToDelete(): List<Long> {
		val idsArray = mutableListOf<Long>()
		realm.executeTransaction {
			it.where(DatabaseModel::class.java)
				.equalTo("toDelete", true)
				.findAll()
				.forEach {
					if (it.remoteId != null) idsArray.add(it.remoteId!!)
				}
		}
		return idsArray
	}

	/**
	 * Получить список заметок для адаптера RecyclerView
	 */
	fun getOrderedRealmCollection(): OrderedRealmCollection<DatabaseModel> =
		realm.where(DatabaseModel::class.java).findAllAsync()
	//endregion
}