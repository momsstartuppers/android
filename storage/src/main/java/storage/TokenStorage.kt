package storage

import android.content.Context
import api.TokenStorage

const val STORAGE_NAME = "TokenStorage"
const val TOKEN_KEY = "tokenKey"

class TokenStorage(private val context: Context) : TokenStorage {

	override fun saveToken(token: String) {
		context
			.getSharedPreferences(STORAGE_NAME, Context.MODE_PRIVATE)
			.edit()
			.putString(TOKEN_KEY, token)
			.apply()
	}

	@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
	override fun getToken(): String {
		return context
			.getSharedPreferences(TOKEN_KEY, Context.MODE_PRIVATE)
			.getString(TOKEN_KEY, "")
	}
}