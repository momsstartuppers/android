package storage

import api.DatabaseStorage
import api.TokenStorage
import memo.shared.model.NoteModel

class Storage(
	private val tokenStorage: storage.TokenStorage,
	private val storage: Database
) : DatabaseStorage, TokenStorage {

	/**
	 * Сохранить заметку в хранилище
	 * @param note - заметка на сохранение
	 * */
	override fun saveNote(note: NoteModel) = storage.saveNote(note)

	/**
	 * Сохранить список заметок в хранилище
	 * @param notes - список заметок на сохранение
	 * */
	override fun saveNotes(notes: List<NoteModel>) = storage.saveNotes(notes)

	/**
	 * Удалить заметку из хранилища
	 * @param id - идентификатор заметки на удаление
	 * */
	override fun deleteNote(id: String) = storage.deleteNote(id)

	/**
	 * Удалить несколько заметок из хранилища
	 * @param ids - список из идентификаторов заметок на удаление
	 * */
	override fun deleteNotes(ids: Array<String>) = storage.deleteNotes(ids)

	/**
	 * Пометить заметку для удаления в хранилище
	 * @param id - идентификатор заметки
	 * */
	override fun markNoteOnDelete(id: String) = storage.markNoteOnDelete(id)

	/**
	 * Пометить заметки для удаления в хранилище
	 * @param ids - список идентификаторов заметок
	 * */
	override fun markNotesOnDelete(ids: Array<String>) = storage.markNotesOnDelete(ids)

	/**
	 * Получить список remoteId заметок на удаление
	 * */
	override fun getNotesIdToDelete(): List<Long> = storage.getNotesIdToDelete()

	/**
	 * Сохранить токен в хранилище
	 * @param token - токен
	 * */
	override fun saveToken(token: String) = tokenStorage.saveToken(token)

	/**
	 * Получить токен из хранилища.
	 * @return - вернёт токен. Если его нет, то вернёт пустую строку
	 * */
	override fun getToken(): String = tokenStorage.getToken()
}