package storage.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.note_cell.view.*
import storage.DatabaseModel
import storage.R
import java.util.*
import kotlinx.android.synthetic.main.note_cell_with_image.view.datetime as imageViewDatetime
import kotlinx.android.synthetic.main.note_cell_with_image.view.image as imageViewPicture
import kotlinx.android.synthetic.main.note_cell_with_image.view.mood as imageViewMood
import kotlinx.android.synthetic.main.note_cell_with_image.view.status as imageViewStatus
import kotlinx.android.synthetic.main.note_cell_with_image.view.text as imageViewText
import kotlinx.android.synthetic.main.note_cell_with_image.view.title as imageViewTitle

class NotesAdapter(data: OrderedRealmCollection<DatabaseModel>) :
	RealmRecyclerViewAdapter<DatabaseModel, NotesAdapter.BaseViewHolder>(data, true) {
	companion object {
		const val VIEW_TYPE_TEXT = 0
		const val VIEW_TYPE_IMAGE = 1

		//TODO вынести в Utils
		private fun dateFormatter(dateTime: Long): String = Date(dateTime).toString()
	}

	override fun getItemViewType(position: Int): Int =
		when {
			data!![position].image == null -> VIEW_TYPE_TEXT
			else -> VIEW_TYPE_IMAGE
		}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesAdapter.BaseViewHolder =
		when (viewType) {
			VIEW_TYPE_TEXT -> {
				TextViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.note_cell, parent, false))
			}
			VIEW_TYPE_IMAGE -> {
				ImageViewHolder(
					LayoutInflater.from(parent.context).inflate(
						R.layout.note_cell_with_image,
						parent,
						false
					)
				)
			}
			else -> throw Exception("Неожиданный viewType")
		}

	override fun onBindViewHolder(holder: NotesAdapter.BaseViewHolder, position: Int) {
		holder.bind(data!![position])
	}

	abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view), NoteBinder

	class TextViewHolder(view: View) : BaseViewHolder(view) {
		private val title = view.title
		private val body = view.text
		private val dateTime = view.datetime
		private val mood = view.mood      //TODO изменять цвет поля в зависимости от значения в заметке
		private val status = view.status //TODO выбрать картинку для поля status

		override fun bind(note: DatabaseModel) {
			title.text = note.title
			body.text = note.text
			dateTime.text = dateFormatter(note.date)
		}
	}

	class ImageViewHolder(view: View) : BaseViewHolder(view) {
		private val title = view.imageViewTitle
		private val body = view.imageViewText
		private val dateTime = view.imageViewDatetime
		private val image = view.imageViewPicture
		private val mood = view.imageViewMood      //TODO изменять цвет поля в зависимости от значения в заметке
		private val status = view.imageViewStatus //TODO выбрать картинку для поля status

		override fun bind(note: DatabaseModel) {
			title.text = note.title
			body.text = note.text
			dateTime.text = dateFormatter(note.date)
			//TODO добавлять картинку
		}
	}

	private interface NoteBinder {
		fun bind(note: DatabaseModel)
	}
}