package memo.diary.network_api.model.response

data class ErrorResponse(
	val responseCode: Int,
	val errorCode: Int,
	val errorMessage: String
)