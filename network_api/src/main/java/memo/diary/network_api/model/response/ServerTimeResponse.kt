package memo.diary.network_api.model.response

data class ServerTimeResponse(
	val serverTime: Long
)