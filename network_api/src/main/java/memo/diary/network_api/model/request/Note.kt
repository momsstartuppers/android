package memo.diary.network_api.model.request

data class Note(
	val date: Long,

	val mood: Int,

	val localId: String,

	val remoteId: Long? = null,

	val text: String,

	val title: String
)