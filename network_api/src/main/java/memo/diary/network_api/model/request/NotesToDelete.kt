package memo.diary.network_api.model.request

data class NotesToDelete(
	val notesToDelete: List<Long>
)