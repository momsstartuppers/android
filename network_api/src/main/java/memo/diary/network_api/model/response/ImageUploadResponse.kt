package memo.diary.network_api.model.response

data class ImageUploadResponse(
	val image: String
)