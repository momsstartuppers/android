package memo.diary.network_api.model.response

import memo.shared.model.NoteModel

data class ListOfNotesResponse(
	val notesId: List<Long?>,

	val updated: List<NoteModel>
)