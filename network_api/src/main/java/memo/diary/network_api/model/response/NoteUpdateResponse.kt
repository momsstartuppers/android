package memo.diary.network_api.model.response

data class NoteUpdateResponse(
	val localId: String,
	val remoteId: Long
)