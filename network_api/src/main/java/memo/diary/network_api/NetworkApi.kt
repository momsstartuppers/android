package memo.diary.network_api

import io.reactivex.Single
import memo.diary.network_api.model.request.Note
import memo.diary.network_api.model.response.*
import memo.shared.model.NoteModel
import java.io.File

interface NetworkApi {
	/**
	 * Возвращает серверное время
	 * */
	fun getServerTime(): Single<NetworkResponse<ServerTimeResponse>>

	/**
	 * Получить заметку с сервера
	 * @param remoteId - id заметки на сервере
	 * */
	fun getNote(remoteId: Long): Single<NetworkResponse<NoteModel>>

	/**
	 * Отправить заметку на сервер
	 * */
	fun postNote(note: Note): Single<NetworkResponse<NoteUpdateResponse>>

	/**
	 * Обновить заметку
	 * @param note - заметка, которую следует обновить
	 * */
	fun updateNote(note: Note): Single<NetworkResponse<NoteUpdateResponse>>

	/**
	 * Удалить заметку с выбранным id
	 * @param remoteId - id заметки на сервере
	 * */
	fun deleteNote(remoteId: Long): Single<NetworkResponse<EmptyResponse>>

	/**
	 * Удалить список заметок
	 * @param notesToDelete - список remoteId заметок на удаление
	 * */
	fun deleteListOfNotes(notesToDelete: List<Long>): Single<NetworkResponse<EmptyResponse>>

	/**
	 * Получить список заметок
	 * @param beginDate - ???
	 * @param endDate - ???
	 */
	fun getListOfNotes(beginDate: Long, endDate: Long): Single<NetworkResponse<ListOfNotesResponse>>

	/**
	 * Загрузить картинку на сервер
	 * @param remoteId - id заметки, хранящейся на сервере
	 * @param image - картинка, загружаемая на сервер
	 * */
	fun uploadImage(remoteId: Long, image: File): Single<NetworkResponse<ImageUploadResponse>>

	/**
	 * Получить картинку с сервера
	 * @param remoteId - id заметки, хранящейся на сервере
	 * */
	fun getImage(remoteId: Long): Single<NetworkResponse<ImageUploadResponse>>

	/**
	 * Удалить картинку с сервера
	 * @param remoteId - id картинки на сервере
	 * */
	fun deleteImage(remoteId: Long): Single<NetworkResponse<EmptyResponse>>
}