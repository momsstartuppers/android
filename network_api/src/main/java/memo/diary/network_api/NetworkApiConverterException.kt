package memo.diary.network_api

import java.lang.Exception

class NetworkApiConverterException(message: String) : Exception(message)