package memo.diary.network_api

data class NetworkResponse<T>(
	val responseCode: Int,
	val model: T
)