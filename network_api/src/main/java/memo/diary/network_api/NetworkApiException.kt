package memo.diary.network_api

import java.lang.Exception

class NetworkApiException(
	message: String,
	val responseCode: Int,
	val errorModel: Int
) : Exception(message)